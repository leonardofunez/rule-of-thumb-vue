const data = [
  {
    main_menu: [
      {
        title: 'Past Trials',
        path: 'past-trials'
      },
      {
        title: 'How It Works',
        path: 'how-it-works'
      }
    ],

    auth_menu: [
      {
        title: 'Log In',
        path: 'log-in'
      },
      {
        title: 'Sign Up',
        path: 'sign-up'
      }
    ],

    secondary_menu: [
      {
        title: 'Terms and Conditions',
        path: '/terms-and-conditions'
      },
      {
        title: 'Privacy Policy',
        path: '/privacy-policy'
      },
      {
        title: 'Contact Us',
        path: '/contact-us'
      }
    ],

    message_box: {
      subtitle: 'Speak Out. Be heard.',
      title: 'Be Counted',
      description: 'Rule of Thumb is quis feugiat sapien nulla volutpat mauris fringilla commodo maximus. Curabitur nec mauris luctus magna tincidunt aliquam eget non tellus.'
    },

    banner: {
      text: 'Is there anyone else you would want us to add?',
      button_text: 'Submit a Name'
    },

    pages: [{
      past_trial : {
        title: 'Past Trials',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.'
      },
      how_it_works : {
        title: 'How It Works',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.'
      },
      contact_us : {
        title: 'Contact Us',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.'
      },
      terms_and_conditions : {
        title: 'Terms and Conditions',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.'
      },
      privacy_policy : {
        title: 'Privacy Policy',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.'
      },
      login : {
        title: 'Log In',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.'
      },
      sign_up : {
        title: 'Sign Up',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.'
      }
    }],

    posts: [{
      id: 1,
      title: 'Pope Francis',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit praesent ullamcorper velit at volutpat laoreet.',
      photo: 'pope.jpg',
      category: 'Entertaiment',
      date: '1 months ago',
      link: 'https://en.wikipedia.org/wiki/Pope_Francis'
    },
    {
      id: 2,
      title: 'Kanye West',
      description: 'Ut ac lacinia tortor. In tellus mauris, suscipit a lacus non, auctor auctor orci ultrices posuere cubilia.',
      photo: 'kanye.jpg',
      category: 'Entertaiment',
      date: '1 months ago',
      link: 'https://es.wikipedia.org/wiki/Kanye_West'
    },
    {
      id: 3,
      title: 'Mark Zuckerberg',
      description: 'Morbi faucibus non turpis vel pellentesque. Aliquam dictum, neque sit amet sodales blandit odio lacus.',
      photo: 'mark.jpeg',
      category: 'Business',
      date: '2 months ago',
      link: 'https://es.wikipedia.org/wiki/Mark_Zuckerberg'
    },
    {
      id: 4,
      title: 'Cristina Fernández de Kirchner',
      description: 'Fusce ac felis vitae arcu maximus ultrices vitae in elit. Ut auctor id tortor in interdum.',
      photo: 'cristina.jpg',
      category: 'Politics',
      date: '2 months ago',
      link: 'https://es.wikipedia.org/wiki/Cristina_Fern%C3%A1ndez_de_Kirchner'
    },
    {
      id: 5,
      title: 'Malala Yousafzai',
      description: 'Mauris quis feugiat sapien. Nulla volutpat mauris fringilla commodo maximus curabitur nec.',
      photo: 'malala.jpg',
      category: 'Entertaiment',
      date: '3 months ago',
      link: 'https://es.wikipedia.org/wiki/Malala_Yousafzai'
    }]
  }
]

export default data