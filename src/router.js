import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import PastTrials from './views/PastTrials.vue'
import HowItWorks from './views/HowItWorks.vue'
import LogIn from './views/LogIn.vue'
import SignUp from './views/SignUp.vue'
import TermsAndConditions from './views/TermsAndConditions.vue'
import PrivacyPolicy from './views/PrivacyPolicy.vue'
import ContactUs from './views/ContactUs.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/past-trials',
      name: 'PastTrials',
      component: PastTrials
    },
    {
      path: '/how-it-works',
      name: 'HowItWorks',
      component: HowItWorks
    },
    {
      path: '/log-in',
      name: 'LogIn',
      component: LogIn
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/terms-and-conditions',
      name: 'TermsAndConditions',
      component: TermsAndConditions
    },
    {
      path: '/privacy-policy',
      name: 'PrivacyPolicy',
      component: PrivacyPolicy
    },
    {
      path: '/contact-us',
      name: 'ContactUs',
      component: ContactUs
    },
    // {
    //   path: '/about',
    //   name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
